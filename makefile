###################################################
#
# Makefile for TruEd-LMS/HTTPWatchdog
# Creator: Kevin H. Patterson
# Created: 2017-05-03
#
###################################################

PROJECT_PATH = ..

include $(PROJECT_PATH)/makefile-pre.inc

CC_OPTIONS += -std=c++0x

LIBRARIES += \
	-lboost_system -lboost_filesystem -lboost_chrono -lboost_date_time \
	-lssl -lcrypto \
	-lPocoFoundation -lPocoZip -lPocoNetSSL -lPocoNet -lPocoCrypto -lPocoUtil \
	-lpthread
	
# INCLUDE += \

EXECUTABLE = HTTPWatchdog

SOURCES = \
	main.cpp \
	cli-utils/CmdLineParamParser.cpp

include $(PROJECT_PATH)/makefile-post.inc
