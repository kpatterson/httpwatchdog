/*
 * main.cpp
 * TruEd LMS - A web-based learning management system for small schools
 *
 *       Created on: March 30, 2016
 *   Primary Author: Kevin H. Patterson
 *     Contributors: N/A
 *
 * Copyright © 2015-2017 Hartland Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * See the LICENSE file for detailed terms of use.
 */

#include <iostream>
#include <fstream>
#include <string>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/URI.h>
#include <Poco/StreamCopier.h>
#include <Poco/Net/NetException.h>
#include <Poco/Thread.h>

#include "cli-utils/CmdLineParamParser.hpp"


bool doRequest( Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response, bool i_quiet );


bool doRequest( Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response, bool i_quiet ) {
	bool GotIt = false;
	try {
		session.sendRequest( request );
		GotIt = true;
	} catch( Poco::Net::ConnectionRefusedException e ) {
		if( !i_quiet )
			std::cerr << "connection refused!" << std::endl;
	} catch( Poco::Net::ConnectionAbortedException e ) {
		if( !i_quiet )
			std::cerr << "connection aborted!" << std::endl;
	} catch( Poco::Net::ConnectionResetException e ) {
		if( !i_quiet )
			std::cerr << "connection reset!" << std::endl;
	} catch( Poco::TimeoutException e ) {
		if( !i_quiet )
			std::cerr << "timeout!" << std::endl;
	} catch(...) {
		if( !i_quiet )
			std::cerr << "unknown std::exception!" << std::endl;
	}

	if( !GotIt )
		return false;

	GotIt = false;
	std::istream* rs = nullptr;
	try {
		rs = &session.receiveResponse( response );
		GotIt = true;
	} catch( Poco::Net::NoMessageException ) {
		if( !i_quiet )
			std::cerr << "no message received before timeout!" << std::endl;
	} catch( Poco::TimeoutException e ) {
		if( !i_quiet )
			std::cerr << "timeout!" << std::endl;
	} catch(...) {
		if( !i_quiet )
			std::cerr << "unknown std::exception!" << std::endl;
	}

	if( !i_quiet )
		std::cout << response.getStatus() << " " << response.getReason() << std::endl;

	if( !GotIt )
		return false;

	return true;
//	if( response.getStatus() != Poco::Net::HTTPResponse::HTTP_UNAUTHORIZED ) {
//		std::ofstream ofs( "Poco_banner.jpg", std::fstream::binary );
//		Poco::StreamCopier::copyStream( *rs, ofs );
//		return true;
//	}

	//it went wrong ?
	return false;
}


int main( int argc, const char* argv[] ) {

	CmdLineParamParser Parser( argc, argv );

	std::cout << "Welcome to HTTPWatchdog!" << std::endl;

	int Interval = std::stoi( Parser.GetParamValue( "--interval" ) );
	int Timeout = std::stoi( Parser.GetParamValue( "--timeout" ) );
	std::string url = Parser.GetParamValue( "--url" );
	int pid = std::stoi( Parser.GetParamValue( "--pid" ) );
	bool quiet = Parser.HasParam( "--quiet" );

	std::cout << "  Ping URL: " << url << std::endl;
	std::cout << "  Ping interval: " << Interval << std::endl;
	std::cout << "  Timeout: " << Timeout << std::endl;
	std::cout << "  PID to Kill: " << pid << std::endl;
	if( quiet )
		std::cout << "  -- quiet mode --" << std::endl;
	
	
	std::cout << std::endl;

	Poco::URI uri( url );

	std::string path( uri.getPathAndQuery() );
	if( path.empty() )
		path = "/";

	Poco::Net::HTTPClientSession session( uri.getHost(), uri.getPort() );
	Poco::Net::HTTPRequest request( Poco::Net::HTTPRequest::HTTP_GET, path, Poco::Net::HTTPMessage::HTTP_1_1 );
	Poco::Net::HTTPResponse response;

	session.setTimeout( Poco::Timespan( Timeout, 0 ) );

	Poco::Thread::sleep( Interval * 1000 );
	if( !quiet ) {
		std::cout << "ping: ";
		std::cout.flush();
	}

	while( doRequest( session, request, response, quiet ) ) {
//		session.reset();
		Poco::Thread::sleep( Interval * 1000 );

		if( !quiet ) {
			std::cout << "ping: ";
			std::cout.flush();
		}
	}

	if( !quiet )
		std::cerr << "Request failed." << std::endl;

	{
		std::cout << "HTTPWatchdog: Killing " << pid << std::endl;

		const std::string KillApp = "/bin/kill";

		std::vector<std::string> KillParams;
		KillParams.push_back( KillApp );
		KillParams.push_back( "-9" );
		KillParams.push_back( std::to_string( pid ) );

		const char* KillArgv[ KillParams.size() + 1 ];

		KillArgv[ KillParams.size() ] = nullptr;
		for( size_t i = 0; i < KillParams.size(); ++i )
			KillArgv[i] = KillParams[i].c_str();

		if( execv( KillApp.c_str(), const_cast<char* const*>( KillArgv ) ) < 0 ) {
			if( !quiet )
				std::cerr << "execv error" << std::endl;
		}
	}

	std::cout << "HTTPWatchdog: Bye bye!" << std::endl;
	
	
	{  ///////////////// Log Kill to File
		std::fstream outfile;
		outfile.open( "watchdog_kills.log", std::fstream::app|std::fstream::out );
		
		std::string t_Time;
		{
			time_t rawtime;
			struct tm * timeinfo;
			char buffer[80];
			
			std::time (&rawtime);
			timeinfo = std::localtime(&rawtime);
			
			std::strftime( buffer,80,"%Y-%m-%d %I:%M:%S",timeinfo );
			std::string str(buffer);
			
			t_Time = str;
		}
		
		outfile << "HTTPWatchdog: Killed PID: " << pid << " at: " << t_Time << std::endl;
		
		outfile.close();
	}

	return 1;
}
